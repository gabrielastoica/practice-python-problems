# Check if two strings are anagrams

from collections import Counter

def check_anagrams(s1, s2):

    #to remove all white spaces and to convert to lower case
    d1 = Counter(''.join(s1.lower().split()))
    d2 = Counter(''.join(s2.lower().split()))

    if set(d1.keys()) == set(d2.keys()):
        for k in d1.keys():
            if k in d2.keys() and d1[k] == d2[k]:
                continue
            else:
                print("{} and {} are not anagrams".format(s1, s2))
                return
        print("{} and {} are anagrams".format(s1, s2))
    else:
        print("{} and {} are not anagrams".format(s1,s2 ))

if __name__=='__main__':

    anagrams = {'A gentleman': 'Elegant man', 'Angel': 'Glean', 'Arc': 'Car', 'Astronomer': 'Moon starer',\
                'Bored': 'Robed', 'Brag': 'Grab', 'Cat': 'Act', 'Cider': 'Cried', 'Conversation': 'Voices rant on',\
                'Dormitory': 'Dirty room', 'Dusty': 'Study', 'Elbow': 'Below', 'Eleven plus two': 'Twelve plus one',\
                'Fourth of July': 'Joyful Fourth', 'Funeral':'Real fun','Inch':'Chin','Listen':'Silent',\
                'Night':'Thing','Save':'Vase','School master':'The classroom','Slot machines':'Cash lost in me',\
                'State':'Taste','Stressed':'Desserts','Tar':'Rat','The eyes':'They see','The Morse Code':'Here come dots',\
                '1234':'4231'}

    not_exactly_anagrams = {'A gentlemxan': 'Elegant man', 'XAngel': 'Glean', 'Arc': 'Carx', 'Astronomer': 'Moon3 starer',\
                'Bored5': 'Robed', 'Bragt': 'Grab', 'Gat': 'Act',}


    for k,v in not_exactly_anagrams.items():
        check_anagrams(k,v)


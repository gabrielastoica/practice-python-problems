# https://www.practicepython.org/exercise/2014/07/05/18-cows-and-bulls.html

from random import randint


def cows_and_bulls(n, guess):
    n = str(n)
    tries = 0

    while True:

        cows = 0
        bulls = 0

        if n == guess:
            tries += 1
            print("You tried {} times".format(tries))
            break
        else:
            for i in range(0, len(n)):
                for j in range(0, len(guess)):
                    if n[i] == inp[j]:
                        if i == j:
                            cows += 1
                        else:
                            bulls += 1
            print("You have {} cows and {} bulls".format(cows, bulls))
            guess = input("Try another guess: ")

if __name__=='__main__':
    n = randint(1000, 9999)
    inp = input("Guess the number: ")
    cows_and_bulls(n, inp)
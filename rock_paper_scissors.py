#Rock paper scissors
p1_wins = {'rock':'scissors', 'scissors':'paper', 'paper':'rock'}
p2_wins = {'scissors':'rock', 'paper':'scissors', 'rock':'paper'}
print("Welcome to the Rock Paper Scissors game!")
print(list(p1_wins.keys()) + list(p1_wins.values()))
while True:
    p1 = input("Player 1: ")
    p2 = input("Player 2: ")
    if p1 in p1_wins.keys() and p1_wins[p1] == p2:
        print("Player 1 wins")
        break
    elif p2 in p2_wins.keys() and p2_wins[p2] == p1:
        print("Player 2 wins")
        break
    else:
        print("No winner yet. Please continue")
        continue
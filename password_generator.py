#https://www.practicepython.org/exercise/2014/05/28/16-password-generator.html
from random import randint, choice
small_caps = 'abcdefghijklmnopqrstuvw'
all_caps = 'ABCDEFGHIJKLMNOPQRSTTUWVXYZ'
digits = '012345678910'
sym = '~!@#$%&-_'


def gen_pass(strength):
    gpass = ''
    intermediary_pass_chars = small_caps + all_caps + digits
    strong_pass_chars = intermediary_pass_chars + sym
    if strength == 'weak':
        for i in range(0, 6):
            gpass += choice(small_caps)
        return gpass
    elif strength == 'intermediate':
        n = randint(8, 16)
        for i in range(n):
            gpass += choice(intermediary_pass_chars)
        return gpass
    elif strength == 'strong':
        n = randint(12, 32)
        for i in range(n):
            gpass += choice(strong_pass_chars)
        return gpass
    else:
        return "Unknown option"


if __name__ == '__main__':
    pass_str = input("Please indicate the password strength: ")
    print(gen_pass(pass_str))

# https://www.practicepython.org/exercise/2014/05/21/15-reverse-word-order.html

inp = input("Type a sentence: ")

#1 Method 1
d = inp.split()
s = ''
for el in d:
    s = el + ' ' + s
print(s)

#2 Method 2
print(' '.join(inp.split()[::-1]))

# Method 3
print(*list(reversed(inp.split())), sep=' ')
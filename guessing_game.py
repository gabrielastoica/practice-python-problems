# https://www.practicepython.org/exercise/2014/04/02/09-guessing-game-one.html
# Prints the number of guesses after each correct answer

from random import randint

n = randint(0,9)
inp = input("Take a guess: ")
tries = 1

while True:
    try:
        inp = int(inp)
        if inp < n:
            tries += 1
            inp = input("You guessed too low. Try again: ")
        elif inp > n:
            tries += 1
            inp = input("You guessed too high. Try again: ")
        elif inp == n:
            print("You guessed right in {} guesses".format(tries))
            tries = 1
            n = randint(0,9)
            inp = input("You guessed right. Play again: ")
    except:
        if inp == 'exit':
            break
        else:
            inp = input("This is not a valid answer. Try again: ")

